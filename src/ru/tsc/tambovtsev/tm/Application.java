package ru.tsc.tambovtsev.tm;

import ru.tsc.tambovtsev.tm.constant.TerminalConst;

public final class Application {

    public static void main(final String[] args) {
        displayWelcome();
        process(args);
    }

    public static void process(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                showError(arg);
                break;
        }
    }

    public static void showError(final String arg) {
        System.err.printf("Error! This argument '%s' not supported... \n", arg);
        System.exit(0);
    }

    public static void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Maksim Tambovtsev");
        System.out.println("E-mail: mtambovtsev@tsconsulting.com");
        System.exit(0);
    }

    public static void showVersion() {
        System.out.println("1.1.0");
        System.exit(0);
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - Show developer info. \n", TerminalConst.ABOUT);
        System.out.printf("%s - Show application version. \n", TerminalConst.VERSION);
        System.out.printf("%s - Show terminal commands. \n", TerminalConst.HELP);
        System.exit(0);
    }

}
